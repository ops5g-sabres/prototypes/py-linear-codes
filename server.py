#!/usr/bin/python3
from concurrent import futures
import logging
import socket

import grpc
import message_pb2
import message_pb2_grpc

selfName = socket.gethostname()

dataWindow = {}
ackNum = 0

def scanWindow():
	global ackNum
	while ackNum in dataWindow:
		print(dataWindow[ackNum])
		del(dataWindow[ackNum])
		ackNum += 1

def handleMessage(request):
	global ackNum
	# if we are the final node in the path
	if request.path[0] == selfName:
		if ackNum == request.order:
			print (request.data)
			ackNum += 1
			scanWindow()
		else:
			dataWindow[request.order] = request.data
	else:
		fwdReq = request
		fwdReq.path = fwdReq.path[1:]

		with grpc.insecure_channel(fwdReq.path[0]+':50051') as channel:
			stub = message_pb2_grpc.MessageStub(channel)
			response = stub.SendMessage(
				message_pb2.MsgRequest(fwdReq)
			)

	return message_pb2.MsgReply()

class Message(message_pb2_grpc.MessageServicer):
	def SendMessage(self, request, context):
		return handleMessage(request)

def serve():
	server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
	message_pb2_grpc.add_MessageServicer_to_server(Message(), server)
	server.add_insecure_port('[::]:50051')
	server.start()
	server.wait_for_termination()


if __name__ == '__main__':
	logging.basicConfig()
	serve()
