#!/usr/bin/python3
from concurrent import futures
import logging
import socket

import grpc
import message_pb2
import message_pb2_grpc

selfName = socket.gethostname()

msg = message_pb2.MsgRequest(
    path = [selfName],
    order = 0,
    data = 'hello',
)

with grpc.insecure_channel('localhost:50051') as channel:
	stub = message_pb2_grpc.MessageStub(channel)
	response = stub.SendMessage(msg)
