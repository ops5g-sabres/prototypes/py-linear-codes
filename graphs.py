#!/usr/bin/python3

import networkx as nx
import matplotlib.pyplot as plt
import random

random.seed(30112)

numNodes = 20

def getRand(notAllowed):
    x = random.randint(0, numNodes)
    while x == notAllowed:
        x = random.randint(0, numNodes)
    return x


nodes = [x for x in range(numNodes)]
G = nx.Graph(labels=nodes)

for node in nodes:
    G.add_node(node)

    # add degree 2 to every node
    G.add_edge(node, getRand(node))
    G.add_edge(node, getRand(node))


# select a random source/sink
s = getRand(None)
t = getRand(s)

print(s,t)

#colors = ['blue' for x in range(numNodes)]
#colors[s-1] = 'green'
#colors[t-1] = 'red'


#subax1 = plt.subplot(121)

# https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.connectivity.disjoint_paths.node_disjoint_paths.html#networkx.algorithms.connectivity.disjoint_paths.node_disjoint_paths
#node_disjoint_paths(G, s, t, flow_func=None, cutoff=None, auxiliary=None, residual=None)
npaths = nx.algorithms.connectivity.disjoint_paths.node_disjoint_paths(G, s, t)

for p in npaths:
    print(p)


epaths = nx.algorithms.connectivity.disjoint_paths.edge_disjoint_paths(G, s, t)

for p in epaths:
    print(p)


#nx.draw(G, node_color=colors, with_labels=True)
nx.draw(G, with_labels=True)
plt.show()
